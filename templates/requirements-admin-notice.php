<?php
/*
 * See $errors array in class ACC_View
 */
?>

<div class="error notice">
    <p><?php _e(
        'The '.ACC_TEMPLATE_PLUGIN_NAME.' plugin will not be functional until the requirements below are met.',
        ACC_TEMPLATE_PLUGIN_TEXTDOMAIN
    ); ?></p>
    
    <ul class="ul-disc">
		<?php foreach ( $errors as $error ) : ?>
			<li>
				<strong><?php echo esc_html( $error->error ); ?></strong>
				<em><?php echo esc_html( $error->description ); ?></em>
			</li>
		<?php endforeach; ?>
	</ul>

</div>
