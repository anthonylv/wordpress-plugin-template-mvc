<?php
/*********************************
 * Plugin Name: ACoCL Plugin Template
 * Plugin URI: http://anothercoffee.net
 * Description: A custom plugin for the ACoCL Plugin Template.
 * Version: 0.1
 * Author: Another Cup of Coffee
 * Author URI: http://anothercoffee.net
 **/

/* 
 * The plugin file structure is as follows.
 * 
 * plugin-directory/
 *  │
 *  ├── plugin-file.php: the main plugin file
 *  │
 *  ├── core/
 *  │    ├── class-*.php: class files for the plugin
 *  │    └── functions.php: helper functions
 *  │
 *  └── templates/
 *       └── user interface templates
 * 
 *
 */

/*
 * For security as specified in
 * http://codex.wordpress.org/Writing_a_Plugin
 * Prevent direct access to this file outside of the WordPress context
 */
if ( ! defined( 'ABSPATH' ) ){ exit; }

/* 
 * Defs
 */
define( 'ACC_TEMPLATE_VERSION', '0.1' );
define( 'ACC_TEMPLATE_REQUIRED_WP_VERSION', '5.0' );
define( 'ACC_TEMPLATE_PLUGIN_TEXTDOMAIN', 'acc_plugin_template_mvc');
define( 'ACC_TEMPLATE_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
define( 'ACC_TEMPLATE_PLUGIN_NAME', trim( dirname( ACC_TEMPLATE_PLUGIN_BASENAME ), '/' ) );
define( 'ACC_TEMPLATE_PLUGIN_DIR', untrailingslashit( dirname( __FILE__ ) ) );
define( 'ACC_TEMPLATE_PLUGIN_URL', untrailingslashit( plugins_url( '', __FILE__ ) ) );

define( 'ACC_TEMPLATE_PLUGIN_CORE_DIR', ACC_TEMPLATE_PLUGIN_DIR.DIRECTORY_SEPARATOR.'core' );
define( 'ACC_TEMPLATE_PLUGIN_TEMPLATES_DIR', ACC_TEMPLATE_PLUGIN_DIR.DIRECTORY_SEPARATOR.'templates' );

/* Helper functions */
require_once( ACC_TEMPLATE_PLUGIN_CORE_DIR.DIRECTORY_SEPARATOR.'functions.php' );
/* Controller will be used for the plugin main */
require_once( ACC_TEMPLATE_PLUGIN_CORE_DIR.DIRECTORY_SEPARATOR.'class-controller.php' );

$ACC_Plugin = new ACC_Controller();
