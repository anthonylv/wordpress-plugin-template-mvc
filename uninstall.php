<?php
/*********************************
 * Clean up the plugin when it is uninstalled
 * 
 * For more information, see:
 * https://developer.wordpress.org/plugins/plugin-basics/uninstall-methods/
 */
 
/*
 * Make sure the user initiating this has the correct persmissions and was
 * referred from another admin page with the correct security nonce.
 */
if ( ! current_user_can( 'activate_plugins' ) ) {
	return;
}
check_admin_referer( 'bulk-plugins' );

if ( __FILE__ != WP_UNINSTALL_PLUGIN ) {
	return;
}

// if uninstall.php is not called by WordPress, die
if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}

// Uninstallation actions here

/*
 * Sample actions to consider
 *
//This will delete ‘my_option’ from the options table within your MySQL database.
$option_name = 'my_option';
delete_option($option_name);
 
// ...or for site options in Multisite
delete_site_option($option_name);

// ...or delete options matching acc_ prefix
global $wpdb;
$wpdb->query( "DELETE FROM {$wpdb->options} WHERE option_name LIKE 'acc_%'" );

// Drop a custom database table
global $wpdb;
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}mytable");

*
*/
