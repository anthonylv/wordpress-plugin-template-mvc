<?php
/**
 * View class for the ACC plugin
 * 
 */

if ( ! class_exists( 'ACC_View' ) ) :

class ACC_View {
    /**
     * Holds Error messages
     */
    private $errors = [];


	/**
	 * 
	 */
	public function __construct() {
	}


    /**
     * Adds Error message in $errors variable
     *
     * @param string $error         Error Message.
     * @param string $description   Description to be displayed along with
     *                              Error Message in brackets.
     */
    private function add_error_notice( $error, $description ) {
        $this->errors[] = (object) [
            'error' => $error_message,
            'description' => $description,
        ];
    }


    /**
     * Display an error if the dependencies check fails.
     * Edit this to show list of dependencies to the user.
     */
    public function show_dependencies_notice() {
        $errors = $this->errors;
        require_once( ACC_TEMPLATE_PLUGIN_TEMPLATES_DIR.'/requirements-admin-notice.php');
    }
}

endif;
