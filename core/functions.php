<?php 
/*********************************
 * Helper functions for this plugin
 */
// Prevent direct access to this file outside of the WordPress context
if ( ! defined( 'ABSPATH' ) ){ exit; }


/*
 * Custom logging
 * Enables us to grep in the logfiles
 */
if ( !function_exists('acc_write_log')) {
   function acc_write_log( $log )  {
      if ( is_array( $log ) || is_object( $log ) ) {
         error_log(print_r( "ACC: ".$log, true ) );
      } else {
         error_log( "ACC: ".$log );
      }
   }
} else {
    error_log("function_exists: acc_write_log");
}


/*
 * Recursively print nested array values
 * e.g. json file containing nested arrays
 */
if ( !function_exists('acc_recursive_array_write_log')) {
    function acc_recursive_array_write_log($arr){
        foreach ($arr as $key => $val) {
            if (is_array($val)) {
                acc_recursive_array_write_log($val);
            } else {
                $print_keyval = "---".$key."[";
                if(is_string($val)) {
                    $print_keyval = $print_keyval.$val;
                } else {
                    $print_keyval = $print_keyval."Cannot print object of type: ".gettype($val);
                }
                acc_write_log($print_keyval."]");
            }
        }
    }
}


/*
 * Check if a string is json
 * 
 * @param   string
 * @return  true if string is json
 */
if ( !function_exists('acc_is_json')) {
    function acc_is_json($string) {
        return is_string($string) &&
            is_array(json_decode($string, true))
            && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }
}


/*
 * Plugin cleanup
 */
register_activation_hook( ACC_TEMPLATE_PLUGIN_BASENAME, 'acc_activation' );
function acc_activation() {
    acc_write_log(ACC_TEMPLATE_PLUGIN_NAME." plugin activated by ".wp_get_current_user()->user_login);
}

register_deactivation_hook( ACC_TEMPLATE_PLUGIN_BASENAME, 'acc_deactivation' );
function acc_deactivation() {
    acc_write_log(ACC_TEMPLATE_PLUGIN_NAME." plugin deactivated ".wp_get_current_user()->user_login);
}
