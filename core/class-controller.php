<?php
/**
 * Controller class for the ACC plugin
 * 
 */ 
require_once( ACC_TEMPLATE_PLUGIN_CORE_DIR.DIRECTORY_SEPARATOR.'class-model.php' );
require_once( ACC_TEMPLATE_PLUGIN_CORE_DIR.DIRECTORY_SEPARATOR.'class-view.php' );


if ( ! class_exists( 'ACC_Controller' ) ) :
class ACC_Controller {
    
    protected $model;
    protected $view;

    /**
     * 
     */
	public function __construct() {
        add_action( 'plugins_loaded', array( $this, 'init' ) );
	}

    /**
     *
     */
    public function init() {
        $ACC_Model = new ACC_Model();
        $ACC_View = new ACC_View();
        
        $this->set_model( $ACC_Model );
        $this->set_view( $ACC_View );
        
        if(!$this->dependencies_met()) {
            add_action( 'admin_notices', array($this->view, 'show_dependencies_notice') );
        } else {
            
        }
	}
    
    /**
     * Check dependencies
     */
    public function dependencies_met() {
        $success = true;
        if ( !class_exists( 'TheDependecy' ) ) {
            $success = false;
        }
        return $success;
    }

    /**
     * Sets the model
     */
    protected function set_model( ACC_Model $model ) {
        $this->model = $model;
    }

    /**
     * Sets the view
     */
    protected function set_view( ACC_View $view ) {
        $this->view = $view;
    }

}
endif;
