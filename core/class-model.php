<?php
/**
 * Model class for the ACC plugin
 * 
 * Sample code included.
 * Responsible for interacting with the:
 * - remote server
 * - WordPress database
 * 
 */

if ( ! class_exists( 'ACC_Model' ) ) :

class ACC_Model {

	public function __construct() {
        // Construct stuff
	}

    /*
     *
     */
    public function send_to_endpoint($order_id) {
        $json_response_array = array();
        $json_response_array["status"] = "UNKNOWN";
        $json_response_array["message"] = "Did not receive a response";
        
        //create an order instance
        $wc_order = wc_get_order($order_id);
        $order_array = $this->order_fields_to_array($wc_order);        
        $json_array = json_encode( $order_array );

        /* Enable to debug WooCommerce fields */
        //$this->debug_wc_order_fields_to_log($wc_order);

        acc_write_log("Sending order to OIDC...");
        $response = $this->endpoint_post($json_array);

        if(acc_is_json($response)) {
            acc_write_log("...received json response");
            $json_response_array = json_decode($response, TRUE );

            if($json_response_array) {
                acc_write_log(
                    "Status: "
                    .$json_response_array["status"]
                    ." Message: "
                    .$json_response_array["message"]
                );
            }
        } else {
            $json_response_array["message"] = $response;
            acc_write_log("Did not receive json response: ".$response);
        }
        
        // Save the response as a postmeta attached to the order.
        // In most cases order_id = post_id but let's make sure.
        $post_id = get_post($order_id)->ID;
        add_post_meta( $post_id, ACC_META_KEY_ENDPOINT_RESPONSE, $json_response_array );
    }
    
    /*
     * Convert the form fields into an array
     * 
     * @param   
     * @return  
     */
    public function form_fields_to_array() {
        $form_fields = array(
            "field1" => "Field 1",
            "field2" => "Field 2",
            "field3" => "Field 3"
        );

        return $form_fields;
    }


    /*
     * POST a request to the OIDC endpoint
     */
    public function endpoint_post($json_array) {
        $endpoint_url = get_option(ACC_OPTION_NAME)[ACC_OPTION_NAME_PURCHASE_ENDPOINT];
        $response_body = "";

        if(acc_is_json($json_array)) {
            acc_write_log("Sending valid json");
            // For request arguments, see
            // https://developer.wordpress.org/reference/classes/WP_Http/request/
            $request_args = array(
                'method'      => 'POST',
                'headers'     => array('Content-Type' => 'application/json; charset=utf-8'),
                'timeout'     => 10,
                'blocking'    => true, // We want to ensure this succeeded
                /* OIDC server requires a JSON array, not string */
                'body'        => $json_array, //wp_json_encode( $json_array ),
                'data_format' => 'body',
            );
            acc_write_log(wp_json_encode( $json_array ));
            $response = wp_remote_post( esc_url_raw( $endpoint_url ), $request_args );

            $response_code = wp_remote_retrieve_response_code( $response );
            $response_body = wp_remote_retrieve_body( $response );
            if ( !in_array( $response_code, array(200,201) ) || is_wp_error( $response_body ) ) {
                acc_write_log("There was an error posting to endpoint ["
                    .$response_code."] ".$response_body);
            } else {
                acc_write_log("200 or 201 posting to endpoint [".$response_code."] ".$response_body);
            }
        } else {
            acc_write_log("Did not recieve json");
        }
        return $response_body;
    }

    /*
     * GET a request from the endpoint
     */
    public function endpoint_get() {
        $json_response_array = null;
        $endpoint_url = get_option(ACC_OPTION_NAME)[ACC_OPTION_NAME_PURCHASE_ENDPOINT];

        $request = wp_remote_get(esc_url_raw( $endpoint_url ));
        
        if( !is_wp_error( $request ) ) {
            $body = wp_remote_retrieve_body( $request );
            
            if(acc_is_json($body)) {
                $json_response_array = json_decode( $body, TRUE );
            } else {
                acc_write_log("Did not recieve json from endpoint");
            }
        } else {
            acc_write_log($request->get_error_message());
        }
        return $json_response_array;
    }

}
endif;
