A custom plugin template.
by Another Cup of Coffee Limited

Author: Anthony Lopez-Vito
Contact: http://anothercoffee.net
Date: 2018-01-01

=== Note ===

This is a template used for plugin development projects. It cannot be used
as-is and must be edited for use.


=== Brief ===

Brief from CLIENT, DATE


=== Installation ===

1) Install and activate the DEPENDENT PLUGINS
2) Install and activate the DEPENDENT THEMES
3) Copy the plugin folder into wp-content/plugins
4) Copy the archive template files to the theme folder:
    * plugins/acc-template/templates/XXX.php -> themes/XXX/


=== Dependencies ===

This plugin has the following dependencies:


=== Troubleshooting === 

Problem:
After activating the plugin, clicking on a CUSTOM TEMPLATE post returns a 404 error.


Solution:
This is a common problem with WordPres custom content types. WordPress' rewrite
will likely need to be flushed to handle the content type's new permalink slugs.

The best way to do this is by following these steps:

1. Go to Dashboard > Settings > Permalinks
2. Make sure Common Settings is set to Post name
3. Click Save Changes

Saving the changes will flush any old rules so make sure you click Save Changes
even if it's already set to Post name.


=== Plugin directory structure ===


  plugin-directory/
   │
   ├── plugin-file.php: the main plugin file
   │
   ├── core/
   │    ├── class-*.php: class files for the plugin
   │    └── functions.php: helper functions
   │
   └── templates/
        └── user interface templates


=== License ===

Unless otherwise specified by the project requirements, this plugin has been
released under the MIT License (MIT)
Copyright (c) 2013 Another Cup of Coffee Limited

See LICENSE.txt 

